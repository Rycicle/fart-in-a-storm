//
//  RSBackgroundNode.h
//  Fart in a Storm
//
//  Created by Ryan Salton on 07/11/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

typedef enum {
    BackgroundTypeStreet,
    BackgroundTypeCount,
} BackgroundType;

@interface RSBackgroundNode : SKNode

@property (nonatomic, assign) BackgroundType type;

- (instancetype)initWithType:(BackgroundType)type andSize:(CGSize)size;
- (void)lightningStrike;
- (void)thunderRumbleWithIntensity:(CGFloat)intensity;

@end
