//
//  GameScene.m
//  Fart in a Storm
//
//  Created by Ryan Salton on 07/11/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import "GameScene.h"
#import "RSBackgroundNode.h"
#import "RSPlayer.h"


@interface GameScene ()

@property (nonatomic, strong) RSBackgroundNode *background;
@property (nonatomic, strong) RSPlayer *player;
@property (nonatomic, strong) RSLifeBar *gasMeter;

@end

@implementation GameScene {
    NSInteger meterSpeed;
    NSInteger meterTimer;
    
    BOOL touching;
}

-(void)didMoveToView:(SKView *)view {
    /* Setup your scene here */
    
    meterTimer = 0;
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.scene.backgroundColor = [UIColor whiteColor];
    
    self.background = [[RSBackgroundNode alloc] initWithType:BackgroundTypeStreet andSize:CGSizeMake(self.size.width, self.size.height)];
    self.background.position = CGPointMake(self.size.width * 0.5, self.size.height * 0.5);
    [self addChild:self.background];
    
    meterSpeed = 3;
    
    self.player = [[RSPlayer alloc] initWithColor:[UIColor greenColor] size:CGSizeMake(100, 140)];
    self.player.position = CGPointMake(self.size.width * 0.5, (self.player.size.height * 0.5) + 20);
    [self addChild:self.player];
    
    self.gasMeter = [[RSLifeBar alloc] initWithColor:[UIColor redColor] size:CGSizeMake(300,40)];
    self.gasMeter.position = CGPointMake(self.size.width * 0.5, self.size.height - (self.gasMeter.size.height * 0.5) - 20);
    [self addChild:self.gasMeter];
    
    self.gasMeter.currAmount = 50;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    /* Called when a touch begins */
    
    touching = YES;
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    touching = NO;
}

-(void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
    meterTimer++;
    
    if (touching)
    {
        self.gasMeter.currAmount --;
    }
    else if (meterTimer % meterSpeed == 0)
    {
        self.gasMeter.currAmount++;
    }
    
    NSInteger randChance = arc4random_uniform(100);
    
    if (randChance == 1)
    {
        [self.background lightningStrike];
    }
    
}

#pragma mark - Life Bar Delegate

- (void)RSLifeBarBecameEmpty
{
    
}

- (void)RSLifeBarBecameFull
{
    
}

@end
