//
//  GameScene.h
//  Fart in a Storm
//

//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "RSLifeBar.h"


@interface GameScene : SKScene <RSLifeBarDelegate>

@end
