//
//  RSLifeBar.m
//  Fart in a Storm
//
//  Created by Ryan Salton on 13/11/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import "RSLifeBar.h"

@protocol RSLifeBarDelegate;

@interface RSLifeBar ()

@property (nonatomic, strong) SKShapeNode *outline;
@property (nonatomic, strong) SKSpriteNode *lifeInner;

@end


@implementation RSLifeBar

- (instancetype)initWithColor:(UIColor *)color size:(CGSize)size
{
    self = [super initWithColor:color size:size];
    
    if (self)
    {
        self.color = [UIColor clearColor];
        
        self.lifeInner = [SKSpriteNode spriteNodeWithColor:color size:CGSizeMake(100, size.height)];
        
        self.lifeInner.position = CGPointMake((self.lifeInner.size.width * 0.5) - (size.width * 0.5), self.lifeInner.position.y);
        
        [self addChild:self.lifeInner];
        
        self.outline = [SKShapeNode shapeNodeWithRect:self.frame cornerRadius:4];
        self.outline.fillColor = [UIColor clearColor];
        self.outline.strokeColor = [UIColor whiteColor];
        self.outline.lineWidth = 4.0;
        [self addChild:self.outline];
    }
    
    return self;
}

- (void)setCurrAmount:(NSInteger)currAmount
{
    if (currAmount < 0)
    {
        [self.delegate RSLifeBarBecameEmpty];
    }
    else if (currAmount < self.size.width)
    {
        _currAmount = currAmount;
        self.lifeInner.size = CGSizeMake(currAmount, self.lifeInner.size.height);
        self.lifeInner.position = CGPointMake((self.lifeInner.size.width * 0.5) - (self.size.width * 0.5), self.lifeInner.position.y);
    }
    else
    {
        [self.delegate RSLifeBarBecameFull];
    }
}

@end
