//
//  RSLifeBar.h
//  Fart in a Storm
//
//  Created by Ryan Salton on 13/11/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>


@protocol RSLifeBarDelegate;

@interface RSLifeBar : SKSpriteNode

@property (nonatomic, assign) id <RSLifeBarDelegate> delegate;
@property (nonatomic, assign) NSInteger currAmount;

@end


@protocol RSLifeBarDelegate <NSObject>

- (void)RSLifeBarBecameEmpty;
- (void)RSLifeBarBecameFull;

@end