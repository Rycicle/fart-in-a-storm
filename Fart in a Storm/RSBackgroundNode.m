//
//  RSBackgroundNode.m
//  Fart in a Storm
//
//  Created by Ryan Salton on 07/11/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import "RSBackgroundNode.h"


@interface RSBackgroundNode ()

@property (nonatomic, strong) SKSpriteNode *image;

@end


@implementation RSBackgroundNode

- (instancetype)initWithType:(BackgroundType)type andSize:(CGSize)size
{
    self = [super init];
    
    if (self)
    {
        self.type = type;
        
        self.image = [SKSpriteNode spriteNodeWithColor:[UIColor purpleColor] size:size];
        [self addChild:self.image];
    }
    
    return self;
}

- (void)setType:(BackgroundType)type
{
    _type = type;
    
    switch (type) {
        case BackgroundTypeStreet:
            
            break;
            
        default:
            break;
    }
    
}

- (void)lightningStrike
{
    SKAction *lightningAction = [SKAction sequence:@[[SKAction fadeAlphaTo:0.2 duration:0.1], [SKAction fadeAlphaTo:1.0 duration:0.5]]];
    [self runAction:lightningAction];
}

- (void)thunderRumbleWithIntensity:(CGFloat)intensity
{
    
}

@end
